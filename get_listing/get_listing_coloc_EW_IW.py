import glob
import xarray as xr
import pandas as pd

import sys, os
sys.path.append("/home1/datawork/rmarquar/utils/calvalsentinel1/mpc-sentinel/mpcsentinellibs/data_collect/")
from match_L1_L2_measurement import getL1SAFEcorrespondingToL2SAFE

import argparse 

def get_esa_L1_path(path):

    procesing_type = ['SLC', 'GRDF', 'GRDH', 'GRDM']
    for procesing in procesing_type:
        L1_path = getL1SAFEcorrespondingToL2SAFE(path, L1_procesing_type = procesing)
        if L1_path:
            return L1_path
    return None


def get_daily_listing(coloc_day_path):
    
    coloc_files = glob.glob(coloc_day_path + '/*/*.nc')
    L2_path_list = list(map(lambda x: xr.open_dataset(x, decode_times=False).attrs['sar_path'].split('/measurement')[0], coloc_files))
    L1_path_list = list(map(get_esa_L1_path, L2_path_list))
    
    acq_mode, _, year, day = coloc_day_path.split('/')[-4:]
    
    df = pd.DataFrame()
    df['coloc_path'] = coloc_files
    df['L2_path'] = L2_path_list
    df['L1_path'] = L1_path_list

    df.to_csv(coloc_day_path + '/df_{}_listing_L1_files_{}_{}.csv'.format(acq_mode, year, day), index=False)
    return df


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Create a listing dataframe associated with each colocation day (EW/IW).')
    parser.add_argument('--input_file', help = 'Colocation day path')

    args = parser.parse_args()
    file_path = args.input_file

    get_daily_listing(file_path)