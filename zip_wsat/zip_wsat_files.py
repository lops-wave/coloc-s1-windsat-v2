import gzip
import glob
import os
import argparse


data_path_uncompressed = '/home/datawork-cersat-public/provider/remss/satellite/l2/coriolis/windsat/bmaps_v07/dm/data'
data_path = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/data_compressed/dm"

def listing_wsat_file():
    
    listing = []
    
    for year in ['2014', '2015', '2016', '2017', '2018', '2019']: # years where both SAR and WindSat data are available
        glob_path = data_path_uncompressed + '/' + year + '/*/*v7.0.1'
        listing += glob.glob(glob_path)
    
    with open("/home/datawork-cersat-project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/data_compressed/listing_uncompressed_wsat_files.txt", "w") as output:
        for path in listing:
            output.write(path + '\n')


def zip_wsat_file(file):

    print(file)
    
    dir_path = data_path + '/' + file.split('/')[-3] + '/' + file.split('/')[-2]
    print(dir_path)

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    
    with open(file, 'rb') as f_in, gzip.open(dir_path + '/' + file.split('/')[-1] + '.gz', 'wb') as f_out:
        f_out.writelines(f_in)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Compress WindSat files in order to make them readable by the REMMS reader.')
    parser.add_argument('--inputfile', help = 'WindSat file path')

    args = parser.parse_args()
    windsat_file = args.inputfile

    zip_wsat_file(windsat_file)