import xarray as xr
import pandas as pd
import numpy as np
import glob
import os
import argparse


def create_dataframe(path_dir):
    
    listing_files = glob.glob('{}/*.nc'.format(path_dir))
    df_coloc = pd.DataFrame()
    
    for file in listing_files:

        ds_coloc = xr.open_dataset(file, decode_times=False)
        lon, lat = ds_coloc.attrs['sar_coordinates (lon, lat)'].split(',')

        new_row = {
            'lon_sar': float(lon[1:]),
            'lat_sar': float(lat[:-1]),
            'lon_wsat': ds_coloc['longitude'].values,
            'lat_wsat': ds_coloc['latitude'].values,
            'date_sar': ds_coloc.attrs['date_sar'],
            'date_wsat': ds_coloc.attrs['date_wsat (mean)'],
            'sar_heading_angle': ds_coloc.attrs['sar_heading_angle'],
            'wsat_orbit_segment': str(ds_coloc['orbit_segment'][0].values),
            'rain_wsat': ds_coloc['rain'][0].values,
            'wdir_wsat': ds_coloc['wdir'][0].values,
            'wspd_wsat': ds_coloc['w-aw'][0].values,
            'sst_wsat': ds_coloc['sst'][0].values,
            'sar_path': ds_coloc.attrs['sar_path'],
            'wsat_path': ds_coloc.attrs['wsat_path'],
            'file_path': file 
        }
        
        df_coloc = pd.concat([df_coloc, pd.DataFrame([new_row])], ignore_index=True)
    path_df = os.path.join(path_dir, 'df_{}.csv'.format(os.path.basename(path_dir)))
    df_coloc.to_csv(path_df, index = False)
    return df_coloc


if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Create a dataframe associated with each SAFE directory where a colocalization has been found.')
	parser.add_argument('--input_directory', help = 'Colocalization directory')

	args = parser.parse_args()
	path_dir = args.input_directory

	create_dataframe(path_dir)