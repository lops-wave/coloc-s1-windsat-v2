import pandas as pd
import geopandas as gpd
import numpy as np
import xarray as xr

import shapely.wkt
import shapely.geometry
import os

import argparse

    
def zone_colocation(polygon, zone):
    
    if zone['geometry'].intersects(polygon).any():
        return True
    return False


def point_colocation(polygon, point):
    
    return point.within(polygon)


def meridian_intersection(polygon, line):

    return line.intersects(polygon)


def get_daily_zone_colocation(path_df_day, zone, column_name):

    df_coloc = pd.read_csv(path_df_day)
    
    coloc_ds_list = list(map(lambda x: shapely.wkt.loads(xr.open_dataset(x, decode_times=False).attrs['sar_swath_polygon']), df_coloc['coloc_path']))
    
    if type(zone) == gpd.geodataframe.GeoDataFrame:
        zone_colocation_list = list(map(lambda x: zone_colocation(x, zone), coloc_ds_list))

    elif type(zone) == shapely.geometry.multipoint.MultiPoint:
        zone_colocation_list = list(map(lambda x: point_colocation(x, zone), coloc_ds_list))

    else:
        #type(zone) == shapely.geometry.linestring.LineString.
        zone_colocation_list = list(map(lambda x: meridian_intersection(x, zone), coloc_ds_list))

    df_coloc[column_name] = zone_colocation_list
    
    df_coloc.to_csv(path_df_day, index=False)
    return df_coloc


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Find if there is a colocation between a given zone and a colocation file.')
    parser.add_argument('--input_file', help = 'Daily colocation dataframe path')

    args = parser.parse_args()
    file_path = args.input_file

    #mediterranean_path = '/home1/datawork/rmarquar/wind_project/colocation/stats/mediterranean_sea/shapefile/iho.shp'
    #zone = gpd.read_file(mediterranean_path)
    
    zone = shapely.geometry.MultiPoint([(42.07, 4.66)])
    
    #zone = shapely.geometry.LineString([(0, -90), (0, 90)])

    get_daily_zone_colocation(file_path, zone, 'lion_gulf_buoy')