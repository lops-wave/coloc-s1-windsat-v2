import xarray as xr
import pandas as pd
import numpy as np
import datetime
import os
import argparse

def create_dataframe(nc_coloc_path):
    
    df_coloc = pd.DataFrame()
    
    ds_coloc = xr.open_dataset(nc_coloc_path, decode_times=False)
    
    if ds_coloc['wdir'].shape == (1, 1, 1):
        df_coloc['lon_wsat'] = ds_coloc['longitude'].values
        df_coloc['lat_wsat'] = ds_coloc['latitude'].values
        df_coloc['wdir_wsat'] = ds_coloc['wdir'].values[0][0]
        df_coloc['rain_wsat'] = ds_coloc['rain'].values[0][0]
        df_coloc['sst_wsat'] = ds_coloc['sst'].values[0][0]
        df_coloc['wspd_wsat'] = ds_coloc['w-aw'].values[0][0]
    
    else:
        wdir_values = ds_coloc['wdir'].stack(x=['longitude','latitude']).where(lambda x: ~np.isnan(x), drop=True).squeeze()
        lon_coloc, lat_coloc = wdir_values.longitude, wdir_values.latitude
        ds_cropped = ds_coloc.sel(longitude = lon_coloc, latitude = lat_coloc, orbit_segment = ds_coloc['orbit_segment'].values[0])
        n = lon_coloc.shape[0]

        df_coloc['lon_wsat'] = lon_coloc.values
        df_coloc['lat_wsat'] = lat_coloc.values
        df_coloc['wdir_wsat'] = wdir_values.values
        df_coloc['rain_wsat'] = ds_cropped['rain'].values
        df_coloc['sst_wsat'] = ds_cropped['sst'].values
        df_coloc['wspd_wsat'] = ds_cropped['w-aw'].values

    
    d = {
        'date_sar': ds_coloc.attrs['date_sar'],
        'date_wsat': ds_coloc.attrs['date_wsat (mean)'],
        'sar_heading_angle': ds_coloc.attrs['sar_heading_angle'],
        'wsat_orbit_segment': str(ds_coloc['orbit_segment'].values[0]),
        'sar_path': ds_coloc.attrs['sar_path'],
        'wsat_path': ds_coloc.attrs['wsat_path'],
        'file_path': nc_coloc_path
    }
    df_coloc = df_coloc.assign(**d)
    
    path_dir = os.path.dirname(nc_coloc_path)
    path_df = os.path.join(path_dir, 'df_{}.csv'.format(os.path.basename(nc_coloc_path.split('.')[0])))
    df_coloc.to_csv(path_df, index = False)
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = 'Create a dataframe associated with each colocation file (EW/IW).')
    parser.add_argument('--input_file', help = 'Colocation path')

    args = parser.parse_args()
    file_path = args.input_file

    create_dataframe(file_path)