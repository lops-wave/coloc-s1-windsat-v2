from coloc_sar_wsat import *

import sys
import datetime
import argparse

if __name__ == "__main__":

	wsat_path = "/home/datawork-cersat-public/provider/remss/satellite/l2/coriolis/windsat/bmaps_v07/"
	wsat_path_compressed = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/coloc/sar-wsat/windsat_compressed/"

	parser = argparse.ArgumentParser(description = 'Generate netcdf files from WindSat and SAR data colocated.')
	parser.add_argument('--input_file', help = 'SAR complete filename')
	parser.add_argument('--time_delta', help = 'Maximum time difference in minutes between the colocated files')

	args = parser.parse_args()
	sar_filename = args.input_file
	time_delta = datetime.timedelta(minutes = int(args.time_delta))

	sar_match_wsat(sar_filename, time_delta)

