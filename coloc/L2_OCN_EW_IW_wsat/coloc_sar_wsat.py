import xarray as xr
import rasterio as rio
import pandas as pd
import numpy as np
from shapely import wkt
from shapely import geometry
import datetime

import sys, os
sys.path.append("/home/datawork-cersat-project/mpc-sentinel1/analysis/s1_data_analysis/coloc/coloc_tools/windsat_reader")
import windsat_daily_v7 as wd7

data_path = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/data_compressed/dm"
save_path = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/coloc_sar"
ref_date = datetime.datetime(1970, 1, 1, 0, 0)


def sar_match_wsat(sar_path, time_delta):
    
    ds_sar = xr.open_dataset(sar_path)

    datesar_str = ds_sar.attrs['firstMeasurementTime'] #ESA 
    datesar_dt = datetime.datetime.strptime(datesar_str,'%Y-%m-%dT%H:%M:%SZ')
    
    born_inf = datesar_dt - time_delta
    born_sup = datesar_dt + time_delta
        
    match_dict = {}
    
    match_dict[datesar_dt] = sar_match_wsat_day(datesar_dt)
    
    if born_sup.day != datesar_dt.day:
        match_dict[born_sup] = sar_match_wsat_day(born_sup)
        
    if born_inf.day != datesar_dt.day:
        match_dict[born_inf] = sar_match_wsat_day(born_inf)
    
    for date in match_dict.keys():

        ds_wsat = match_dict[date]
        
        if ds_wsat is not None:
            
            if is_crossing_meridian(ds_sar['owiLon']):
                print('meridian')
                sar_polygon = get_footprint(ds_sar['owiLon'], ds_sar['owiLat'])
                ds_wsat = ds_wsat.assign_coords(longitude = (((ds_wsat.longitude + 180) % 360) - 180)).sortby('longitude')
                ds_wsat_intersected = geographic_intersection_wsat(ds_wsat, sar_polygon, True)
                ds_wsat_colocalized = sar_match_wsat_time(ds_wsat_intersected, datesar_dt, date, time_delta)
                ds_wsat_colocalized = ds_wsat_colocalized.assign_coords(longitude = ds_wsat_colocalized.longitude%360)
                
            else:
                sar_polygon = get_footprint(ds_sar['owiLon']%360, ds_sar['owiLat'])
                ds_wsat_intersected = geographic_intersection_wsat(ds_wsat, sar_polygon, False)
                ds_wsat_colocalized = sar_match_wsat_time(ds_wsat_intersected, datesar_dt, date, time_delta)
            
            ds_result = check_colocalized_data(ds_wsat_colocalized)
            if ds_result is not None:

                format_result(ds_result, ds_sar, sar_path, datesar_dt, date, time_delta, sar_polygon)
                result_to_netcdf(ds_result, time_delta, sar_path)
                return ds_result

        else:
            pass
             
            
def result_to_netcdf(ds_result, time_delta, sar_path):
    
    wsat_filename = os.path.basename(ds_result.attrs['wsat_path'])
    sar_filename = os.path.basename(sar_path)
    result_filename = os.path.splitext(sar_filename)[0] + "_" + os.path.splitext(wsat_filename)[0] +".nc"
    
    l = sar_path.split('/')
    idx = l.index('data')
    final_path = save_path + '/' + str(time_delta.seconds//60) + '/' + os.path.join(*l[idx +1 :idx + 9]).split('.SAFE')[0]
    
    os.makedirs(final_path, exist_ok = True)
    print(final_path)
    ds_result.to_netcdf(path = os.path.join(final_path, result_filename))

    
def format_result(ds_result, ds_sar, sar_path, datesar_dt, date, time_delta, sar_polygon):
    
    convert_boolean_attrs(ds_result)
    ds_result.attrs['sar_path'] = sar_path   
    ds_result.attrs['sar_source_product'] = ds_sar.attrs['sourceProduct']
    ds_result.attrs['date_sar'] = ds_sar.attrs['firstMeasurementTime'].replace('T',' ').replace('Z','')
    ds_result.attrs['date_wsat (mean)'] = str(get_wsat_date(ds_result['mingmt'], date))
    ds_result.attrs['maximum_time_delta (mn)'] = str(int(time_delta.total_seconds() / 60))
    ds_result.attrs['sar_swath_polygon'] = str(sar_polygon)
    ds_result.attrs['sar_heading_angle'] = ds_sar['owiHeading'].values.mean()
    
    
def convert_boolean_attrs(ds_result):
    
    ds_result['land'].attrs['valid_min'] = 0
    ds_result['land'].attrs['valid_max'] = 1
    ds_result['land'].attrs['units'] = '0 or 1'

    ds_result['ice'].attrs['valid_min'] = 0
    ds_result['ice'].attrs['valid_max'] = 1
    ds_result['ice'].attrs['units'] = '0 or 1'

    ds_result['nodata'].attrs['valid_min'] = 0
    ds_result['nodata'].attrs['valid_max'] = 1
    ds_result['nodata'].attrs['units'] = '0 or 1'
    

def get_wsat_date(mingmt, date):
    mean_mingmt = mingmt.mean(skipna=True)
    if mean_mingmt == 1440:
        return datetime.datetime.combine(date, datetime.time.min) + datetime.timedelta(days = 1)
    
    else:
        hours = int(mean_mingmt//60)
        minutes = int(mean_mingmt%60)

        return date.replace(hour = hours, minute = minutes, second = 0)
    
    
def sar_match_wsat_day(date):
    
    end_file = f"{date.month:02d}{date.day:02d}v7.0.1"
    wsat_filename = "wsat_{}{}.gz".format(str(date.year), end_file)
    wsat_path = data_path + "/{}/{}/{}".format(str(date.year), str(date.strftime('%j')), wsat_filename)
    
    if os.path.exists(wsat_path):

        try :
            ds_wsat = wd7.WindSatDaily(wsat_path, np.nan)
            ds_wsat = to_xarray_dataset(ds_wsat)
            ds_wsat.attrs['wsat_path'] = wsat_path
            return ds_wsat

        except Exception as e:
            print("Reading WindSat file failed - ", e, wsat_filename)
            return None
        
    else:
        return None
    
    
def sar_match_wsat_time(ds_wsat_intersected, datesar_dt, day_date, time_delta):

    minutes_since_1970 = (datetime.datetime.combine(day_date, datetime.time.min) - ref_date).total_seconds()/60
    mingmt_converted = ds_wsat_intersected['mingmt'].copy(deep=False) + minutes_since_1970
    time_delta_minutes = time_delta.total_seconds()/60
    diff_datesar = (datesar_dt - ref_date).total_seconds()/60
    
    ds_wsat_dt = ds_wsat_intersected.where((mingmt_converted > diff_datesar - time_delta_minutes) & (mingmt_converted < diff_datesar + time_delta_minutes))
    
    return ds_wsat_dt.dropna('longitude', how = 'all')


def check_colocalized_data(ds_wsat_colocalized):
    
    if ds_wsat_colocalized.longitude.size == 0 or np.all(np.isnan(ds_wsat_colocalized.wdir.values)):
        return None
    
    else:
        ds_wsat_colocalized = ds_wsat_colocalized.where(~np.isnan(ds_wsat_colocalized['wdir']), drop=True)
        return ds_wsat_colocalized
    

def geographic_intersection_wsat(ds_wsat, sar_polygon, cross_meridian):
    
    rasterized = rasterize_polygon(sar_polygon, cross_meridian)
    
    ds_wsat = ds_wsat.where(rasterized)
    
    ds_wsat = ds_wsat.dropna('longitude', how = 'all')
    ds_wsat = ds_wsat.dropna('latitude', how = 'all')
    
    return ds_wsat


def is_crossing_meridian(lon):
    
    lon = lon%360
    lon_min = np.min(lon)
    lon_max = np.max(lon)
    
    if ((lon_min < 15) & (lon_max > 345)):
        return True
    return False


def rasterize_polygon(polygon, cross_meridian):
    
    if cross_meridian:
        bounds = (-179.875, -89.875, 179.875, 89.875)
    else:
        bounds = (0.125, -89.875, 358.75, 89.875)
        
    resolution = 0.25
    transform = rio.Affine.translation(bounds[0],bounds[1]) * rio.Affine.scale(resolution, resolution)
    
    return rio.features.rasterize(shapes = [polygon], out_shape = [720, 1440], transform = transform)


def get_footprint(lon_sar,lat_sar):
    cl = [[0,0], [0,-1], [-1,-1], [-1,0], [0,0]]
    ca = [[0,0], [0,-1], [-1,-1], [-1,0], [0,0]]
    for i in range(len(cl)):
        while np.isnan(lon_sar[cl[i][0], cl[i][1]]):
            cl[i][0] += np.copysign(1, cl[i][0])
            cl[i][1] += np.copysign(1, cl[i][1])
        
        while np.isnan(lat_sar[ca[i][0], ca[i][1]]):
            ca[i][0] += np.copysign(1, ca[i][0])
            ca[i][1] += np.copysign(1, ca[i][1])
    polygon = geometry.MultiPoint([(lon_sar[cl[0][0],cl[0][1]], lat_sar[ca[0][0],ca[0][1]]),
                                 (lon_sar[cl[1][0],cl[1][1]], lat_sar[ca[1][0],ca[1][1]]),
                                 (lon_sar[cl[2][0],cl[2][1]], lat_sar[ca[2][0],ca[2][1]]), 
                                 (lon_sar[cl[3][0],cl[3][1]], lat_sar[ca[3][0],ca[3][1]]),
                                 (lon_sar[cl[4][0],cl[4][1]], lat_sar[ca[4][0],ca[4][1]])]).convex_hull
    return polygon


def to_xarray_dataset(raw):
    def to_tuple(var):
        dims = var.coordinates
        data = var  # the Variable class is a subclass of ndarray
        attrs = {name: getattr(var, name) for name in raw._attributes() if name not in ["coordinates"]}
        return dims, data, attrs
    
    all_vars = {
        name: to_tuple(var)
        for name, var in raw.variables.items()
    }
    coordinate_names = raw._coordinates()
    data_vars = {k: v for k, v in all_vars.items() if k not in coordinate_names}
    coords = {k: v for k, v in all_vars.items() if k in coordinate_names}
    if "orbit_segment" not in coords:
        coords["orbit_segment"] = ("orbit_segment", ["descending", "ascending"])

    ds = xr.Dataset(coords=coords, data_vars=data_vars)
    ds.encoding["source"] = raw.filename
    
    return ds