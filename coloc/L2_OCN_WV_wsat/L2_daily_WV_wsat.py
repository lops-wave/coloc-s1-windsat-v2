import xarray as xr
import rasterio as rio
import numpy as np
from pyproj import Geod
import datetime

import sys, os
sys.path.append("/home/datawork-cersat-project/mpc-sentinel1/analysis/s1_data_analysis/coloc/coloc_tools/windsat_reader")
import windsat_daily_v7 as wd7

data_path = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/data_compressed/dm"
save_path = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/project_rmarquar/wsat/coloc_sar"
ref_date = datetime.datetime(1970, 1, 1, 0, 0)


def sar_daily_match_wsat(sar_daily_path, time_delta):
    
    ds_daily_wv = xr.open_dataset(sar_daily_path)
    datesar_dt = ds_daily_wv['fdatedt'][0].values.astype('datetime64[s]').astype(datetime.datetime)
    
    wsat_dict = {}
    
    wsat_dict['current_day'] = sar_match_wsat_day(datesar_dt)
    wsat_dict['day_sup'] = sar_match_wsat_day(datesar_dt - datetime.timedelta(days=1))
    wsat_dict['day_inf'] = sar_match_wsat_day(datesar_dt + datetime.timedelta(days=1))
    
    n = ds_daily_wv['fdatedt'].shape[0]
    for i in range(n):
        
        current_datesar_dt = ds_daily_wv['fdatedt'][i].values.astype('datetime64[s]').astype(datetime.datetime)
        born_inf = current_datesar_dt - time_delta
        born_sup = current_datesar_dt + time_delta
        match_dict = {}
    
        match_dict[current_datesar_dt] = wsat_dict['current_day']

        if born_sup.day != datesar_dt.day:
            match_dict[born_sup] = wsat_dict['day_sup']

        if born_inf.day != datesar_dt.day:
            match_dict[born_inf] = wsat_dict['day_inf']

        for date in match_dict.keys():

            ds_wsat = match_dict[date]

            if ds_wsat is not None:
                
                lon_sar, lat_sar = ds_daily_wv['lon'][i]%360, ds_daily_wv['lat'][i]
                if np.isnan(lon_sar) or np.isnan(lat_sar):
                    pass
                ds_wsat_intersected = geographic_intersection_wsat(ds_wsat, lon_sar, lat_sar)
                ds_wsat_colocalized = sar_match_wsat_time(ds_wsat_intersected, current_datesar_dt, date, time_delta)
                
                ds_result = check_colocalized_data(ds_wsat_colocalized, lon_sar, lat_sar)
                
                if ds_result is not None:
                    sar_path = get_sar_path(ds_daily_wv['subpath'].values[i], current_datesar_dt)
                    ds_sar = ds_daily_wv[['subpath_l1', 'oswHeading']].sel(fdatedt = ds_daily_wv['fdatedt'][i])
                    format_result(ds_result, ds_sar, sar_path, current_datesar_dt, date, time_delta, (float(lon_sar.values), float(lat_sar.values)))
                    result_to_netcdf(ds_result, time_delta, sar_path)
                    
            else:
                pass
    
        
def sar_match_wsat_day(date):
    
    end_file = f"{date.month:02d}{date.day:02d}v7.0.1"
    wsat_filename = "wsat_{}{}.gz".format(str(date.year), end_file)
    wsat_path = data_path + "/{}/{}/{}".format(str(date.year), str(date.strftime('%j')), wsat_filename)
    
    if os.path.exists(wsat_path):

        try :
            ds_wsat = wd7.WindSatDaily(wsat_path, np.nan)
            ds_wsat = to_xarray_dataset(ds_wsat)
            ds_wsat.attrs['wsat_path'] = wsat_path
            return ds_wsat

        except Exception as e:
            print("Reading WindSat file failed - ", e, wsat_filename)
            return None
    
    else:
        return None
    
    
def geographic_intersection_wsat(ds_wsat, lon_sar, lat_sar):

    return ds_wsat.sel(longitude = lon_sar, latitude = lat_sar, method = 'nearest')


def sar_match_wsat_time(ds_wsat_intersected, datesar_dt, day_date, time_delta):

    minutes_since_1970 = (datetime.datetime.combine(day_date, datetime.time.min) - ref_date).total_seconds()/60
    mingmt_converted = ds_wsat_intersected['mingmt'].copy(deep=False) + minutes_since_1970
    time_delta_minutes = time_delta.total_seconds()/60
    diff_datesar = (datesar_dt - ref_date).total_seconds()/60
    
    ds_wsat_dt = ds_wsat_intersected.where((mingmt_converted > diff_datesar - time_delta_minutes) & (mingmt_converted < diff_datesar + time_delta_minutes))
    
    return ds_wsat_dt


def check_colocalized_data(ds_wsat_colocalized, lon_sar, lat_sar):
    
    ds_wsat_colocalized = ds_wsat_colocalized.dropna('orbit_segment', how = 'all')

    if ds_wsat_colocalized.orbit_segment.size == 0 or np.isnan(ds_wsat_colocalized['wdir'].values[0]):
        return None
    
    
    else:
        lon_wsat, lat_wsat = ds_wsat_colocalized['longitude'], ds_wsat_colocalized['latitude']
        wgs84_geod = Geod(ellps = 'WGS84')
        _, _, dist = wgs84_geod.inv(lon_wsat, lat_wsat, lon_sar, lat_sar)
        
        if dist > 10e3:
            return None
        
        else:
            return ds_wsat_colocalized
        
            
def result_to_netcdf(ds_result, time_delta, sar_path):
    
    wsat_filename = os.path.basename(ds_result.attrs['wsat_path'])
    sar_filename = os.path.basename(sar_path)
    result_filename = os.path.splitext(sar_filename)[0] + "_" + os.path.splitext(wsat_filename)[0] +".nc"
    
    l = sar_path.split('/')
    idx = l.index('data')
    final_path = save_path + '/' + str(time_delta.seconds//60) + '/' + os.path.join(*l[idx +1 :idx + 9])[:-5]
    
    os.makedirs(final_path, exist_ok = True)
    ds_result.to_netcdf(path = os.path.join(final_path, result_filename))

    
def format_result(ds_result, ds_sar, sar_path, datesar_dt, date, time_delta, sar_coords):

    convert_boolean_attrs(ds_result)
    ds_result.attrs['sar_path'] = sar_path   
    ds_result.attrs['sar_source_product'] = str(ds_sar['subpath_l1'].values)
    ds_result.attrs['date_sar'] = str(ds_sar['fdatedt'].values).replace('T', ' ').split('.0')[0]
    ds_result.attrs['date_wsat (mean)'] = str(get_wsat_date(ds_result['mingmt'], date))
    ds_result.attrs['maximum_time_delta (mn)'] = str(int(time_delta.total_seconds() / 60))
    ds_result.attrs['sar_coordinates (lon, lat)'] = str(sar_coords)
    ds_result.attrs['sar_heading_angle'] = float(ds_sar['oswHeading'].values)
    

def convert_boolean_attrs(ds_result):
    
    ds_result['land'].attrs['valid_min'] = 0
    ds_result['land'].attrs['valid_max'] = 1
    ds_result['land'].attrs['units'] = '0 or 1'

    ds_result['ice'].attrs['valid_min'] = 0
    ds_result['ice'].attrs['valid_max'] = 1
    ds_result['ice'].attrs['units'] = '0 or 1'

    ds_result['nodata'].attrs['valid_min'] = 0
    ds_result['nodata'].attrs['valid_max'] = 1
    ds_result['nodata'].attrs['units'] = '0 or 1'


def get_wsat_date(mingmt, date):
    
    mean_mingmt = mingmt.mean()
    
    if mean_mingmt == 1440:
        return datetime.datetime.combine(date, datetime.time.min) + datetime.timedelta(days = 1)
    
    else:
        hours = int(mean_mingmt//60)
        minutes = int(mean_mingmt%60)

        return date.replace(hour = hours, minute = minutes, second = 0)
    

def get_sar_path(subpath, date):
    
    sat_dict = {'S1A': 'sentinel-1a', 'S1B': 'sentinel-1b'}
    safe_dir, nc_file = subpath.split('/')
    sat = safe_dir[0:3]
    year = date.year
    day_of_year = date.strftime('%j')
    path = '/home/datawork-cersat-project/mpc-sentinel1/data/esa/{}/L2/WV/{}_WV_OCN__2S/{}/{}/{}/measurement/{}'.format(sat_dict[sat], sat, year, day_of_year, safe_dir, nc_file)
    
    return path
    
    
def to_xarray_dataset(raw):
    def to_tuple(var):
        dims = var.coordinates
        data = var  # the Variable class is a subclass of ndarray
        attrs = {name: getattr(var, name) for name in raw._attributes() if name not in ["coordinates"]}
        return dims, data, attrs
    
    all_vars = {
        name: to_tuple(var)
        for name, var in raw.variables.items()
    }
    coordinate_names = raw._coordinates()
    data_vars = {k: v for k, v in all_vars.items() if k not in coordinate_names}
    coords = {k: v for k, v in all_vars.items() if k in coordinate_names}
    if "orbit_segment" not in coords:
        coords["orbit_segment"] = ("orbit_segment", ["descending", "ascending"])

    ds = xr.Dataset(coords=coords, data_vars=data_vars)
    ds.encoding["source"] = raw.filename
    
    return ds